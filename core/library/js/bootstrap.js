
//
// document.title = 'Salao Clound v1.0';
//
// salao.auth().onAuthStateChanged(function (user) {
//     if (user) {
//         console.log('logado');
//     } else {
//         console.log('não logado');
//     }
// });
var BOOTSTRAP = {
    _controller: null,
    _model: null,
    _language: [],
    _lang: 'pt_br',
    config: {},
    init: function () {
        document.title = 'Sãlao Clound v1.0';
        if (localStorage.getItem('lang')) {
            this._lang = localStorage.getItem('lang');
        } else {
            localStorage.setItem('lang', this._lang);
            this._lang = localStorage.getItem('lang');
        }

        this.config.button_color = 'white';
        this.config.button_backgroundColor = '#FF1457';
    },
    layout: function () {
        document.body.style.backgroundColor = '#f24355';
        var cssButtons = document.querySelectorAll('button');
        for (btn in cssButtons) {
            if (!isNaN(btn)) {
                cssButtons[btn].style.color = BOOTSTRAP.config.button_color;
                cssButtons[btn].style.backgroundColor = BOOTSTRAP.config.button_backgroundColor;
            }
        }
    }
    , loading: function () {
        borderSize = 5;

        loader = document.createElement('div');
        loader.id = 'loader';
        loader.className = 'loader_in';
        loader_span = document.createElement('span');
        loader.appendChild(loader_span);

        loader.style.position = 'fixed';
        loader.style.zindex = 1000;
        loader.style.top = '50%';
        loader.style.left = '50%';
        loader_span.style.border = borderSize + 'px solid #FF1457';
        loader_span.style.borderTop = borderSize + 'px solid #FFFFFF';
        document.body.appendChild(loader);

        loaderWidth = loader.clientWidth;
        loaderHeight = loader.clientHeight;
        loader.style.margin = '-' + (loaderHeight / 2) + 'px auto auto -' + (loaderWidth / 2) + 'px';
        BOOTSTRAP.loader = loader;
    },
    autoload: function (_name_) {
        var controller = document.createElement("script");
        var d = new Date();
        controller.src = 'core/controller/' + _name_ + '.js?' + d.getTime();
        controller.addEventListener('load', function (data) {
            BOOTSTRAP._controller = eval(_name_ + 'Controller').init();
        });
        document.head.appendChild(controller);

        var model = document.createElement("script");
        model.src = 'core/model/' + _name_ + '.js?' + d.getTime();
        model.addEventListener('load', function (data) {
            BOOTSTRAP._model = eval(_name_ + 'Model').init();
        });

        model.addEventListener('DOMError', function (data) {
            console.log(data);
        });
        document.head.appendChild(model);
    },
    language: function () {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (e) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                BOOTSTRAP._language = JSON.parse(xhr.response);
            }
        }
        var xhrTime = new Date();
        xhr.open("GET", "core/language/" + BOOTSTRAP._lang + '.json?' + xhrTime.getTime(), true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.send();
    },
    render: function (view, name) {
        var xhr = new XMLHttpRequest();
        xhr.onloadstart = function (e) {
            BOOTSTRAP.loader.className = 'fade_in';
        }
        xhr.onloadend = function (e) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var html = xhr.response;
                for (lang in BOOTSTRAP._language) {
                    html = html.replaceAll('{{' + lang + '}}', eval('BOOTSTRAP._language.' + lang));
                }
                document.body.querySelector('form').innerHTML = html;
                document.body.querySelector('form').className = 'fade_in';
                BOOTSTRAP.layout();
                var buttons = document.querySelectorAll('button');
                for (btn in buttons) {
                    if (!isNaN(btn)) {
                        var nameFunction = buttons[btn].dataset.click
                        buttons[btn].addEventListener('click', eval('BOOTSTRAP._controller.' + nameFunction));
                    }
                }
                BOOTSTRAP.loader.className = 'fade_out';
            }
        }
        var xhrTime = new Date();
        xhr.open("GET", "core/view/" + view + '/' + name + '.html?' + xhrTime.getTime(), true);
        xhr.setRequestHeader('Content-type', 'text/html');
        xhr.send();
    },
    setLang: function (lang) {
        localStorage.setItem('lang', lang);
        document.location.reload();
    }
}

String.prototype.replaceAll || (function () {
    var regMetaChars = /[-\\^$*+?.()|[\]{}]/g;
    String.prototype.replaceAll = function (needle, replacement) {
        return this.replace(new RegExp(needle.replace(regMetaChars, '\\$&'), 'g'), replacement);
    };
}());