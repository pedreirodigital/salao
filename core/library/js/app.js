document.addEventListener("DOMContentLoaded", function (event) {
    BOOTSTRAP.init();
    BOOTSTRAP.autoload('firebase');
    BOOTSTRAP.autoload('auth');
    BOOTSTRAP.loading();
    BOOTSTRAP.language();

    setTimeout(function () {
        authController.validade();
    }, 500);
});